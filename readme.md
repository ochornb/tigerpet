# Vanilla HTML, CSS, and JavaScript Pet Sim
- - - -
Play it [here](https://ochornb.bitbucket.io/tigerpet/)
----
![preview](https://bitbucket.org/ochornb/ochornb.bitbucket.io/raw/56b21ab9623f3d1f9893bcad3709a54bcd926acb/portfolio/imgs/tigerpet.jpg)

## **Tools used**
- - - -
* HTML, CSS, JavaScript
* Bitbucket
* Clip Studio Paint
* Inkscape

## **Summary**
- - - -
Back in October 2018 I wanted to learn more about Flexbox, Grids, and responsive web design and came up with a fun way to use them by creating a pet simulator! Kind of like the old-school Tomogachis. 

It took me a few months, finding free time around a full-time job to complete the prototype. 
I learned a lot about Flexbox and Grids during this time and am quite happy how it turned out!

I think one problem I had was that I was building for the computer screen first and trying to make it mobile friendly afterwards. Now I know why building 'mobile-first' is important!

## **Author**
- - - -
* **Olivia Hornback** - *Front End Web Developer* - [Website](https://ochornb.bitbucket.io/) | [LinkedIn](https://linkedin.com/in/oliviahornback)



