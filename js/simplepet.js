/* Don't be frustrated with how little you do in a session, it takes one step at a time and we'll get there. 
   Also don't be frustrated when things don't work the first time after you coded them!*/

  function Species(name, baseImg, happyImg, sadImg) {
    //Species function to allow more pets in the future
    this.speciesName = name;
    this.happiness = 100;
    this.fullness = 100;
    this.play = 100;
    this.world = "forest";
    this.emoteState = 'neutral';
    this.img = [baseImg, happyImg, sadImg];

    //class functions
    //grab some values from the DOM
    var happyBar = document.getElementById('happy-bar');
    var fullnessBar = document.getElementById('hunger-bar');
    var playBar = document.getElementById('play-bar');
    var petSprite = document.getElementById('pet_sprite');

    this.setBaseStats = function () {
        //have the bars reflect the base stats
        happyBar.value = this.happiness;
        fullnessBar.value = this.fullness;
        playBar.value = this.play;
    },
    this.checkEmoteState = function() {
        //change the emote state based on values of happiness, fullness, and play
        //do something very basic to get started

        if (happyBar.value <= 30)
        {   
            //change emote state to sad
            emoteState = 'sad';
            //change img to sad sprite
            this.setImage(emoteState);
        }
        else if (happyBar.value >= 50 && happyBar.value < 80)
        {
            //change emote state to neutral
            emoteState = 'neutral';
            //petSprite.src = this.img[0];
            this.setImage(emoteState);
        }
        else if (happyBar.value >= 90){
            //change emote to happy!!
            emoteState = 'happy';
            this.setImage(emoteState);
        }
    },

    this.happinessCalculator = function (){
        //BASIC CALC
        //a mix of play and fullness to determine final value of happiness
        //get play and fullness value from webpage? should i do it from here lol
        var checkFullness = this.getFullness();
        var checkPlay = this.getPlay();

        //work on values later
        
        if (checkFullness >= 80 && checkPlay >= 80){
            console.log("Fullness is " + checkFullness + "and play is " + checkPlay);
            this.setHappiness(100); //set to max happiness!
        }
        else if (checkFullness >= 50 && checkPlay >= 50){
            this.setHappiness(50);  //set to neutral happiness
        }
        else{
            this.setHappiness(0);       //oh no, we're sad
        }
    },

    this.setImage = function(value) {
        switch(value) {
            case 'sad':
            petSprite.src = this.img[2];
            break;

            case 'neutral':
            petSprite.src = this.img[0];
            break;

            case 'happy':
            petSprite.src = this.img[1];
            break;

            default:
            petSprite.src = this.img[0];
        }
    },

    //getter methods
    //player should not have access to these
    this.getHappiness = function () {
        return this.happiness;
    },

    this.getFullness = function () {
        return this.fullness;
    },

    this.getPlay = function () {
        return this.play;
    },

    this.getPlayMax = function() {
        return playBar.max;
    },

    this.getFullnessMax = function() {
        return fullnessBar.max;
    }

    //setter methods
    this.setHappiness = function (value) {
        this.happiness = value;     //update the var value
        happyBar.value= value;           //update the bar value
    },

    this.setFullness = function (value){
        this.fullness = value;
        fullnessBar.value = value;
    },

    this.setPlay = function (value){
        this.play = value;
        playBar.value = value;
    };
}

//Set up the TigerDog pet
var tigerDog = new Species('TigerDog', './images/simplepet_tigersprite_base_lg.png', './images/simplepet_tigersprite_happy_lg.png', './images/simplepet_tigersprite_sad_lg.png');
console.log(tigerDog.speciesName);

function becomeSad() {
    //This function checks the happiness value of the pet and if it's below a threshold,
    //change the image sprite of the pet, set the state to sad
    var happyBar = document.getElementById('happy-bar');
    if (happyBar.value < 30){
        //change emote state to sad
        var petSprite = document.getElementById('pet_sprite');
        petSprite.src = './images/simplepet_tigersprite_sad_lg.png';
    }
}
//Emote Testing
function testSad()
{
    var happyBar = document.getElementById('happy-bar');
    tigerDog.setFullness(0);
    tigerDog.setPlay(0);
}

function testNeutral() {
   // var happyBar = document.getElementById('happy-bar');
   // happyBar.value = 50;
   tigerDog.setFullness(50);
   tigerDog.setPlay(50);
}

function testHungry() {
    tigerDog.setFullness(10);
}

function timerCheckEmoteState() {
    tigerDog.checkEmoteState();
}

function happyFlash() {

    tigerDog.setImage('happy');                 //everytime we feed or play, make the happy image for a second!
    setTimeout(timerCheckEmoteState, 1000);     //then reset the image to actual happiness level
}

//functions to react on button click (migrate to pet obj later)
function feedPet() {
    //called in foodbutton on event, increase fullness on click
    //not sure if it's possible to go over max but just in case
   
    var fullness = tigerDog.getFullness();
    var fullnessMax = tigerDog.getFullnessMax();

    if (fullness < fullnessMax) {
        fullness += 15;
        tigerDog.setFullness(fullness);
    }
    else {
        fullness = fullnessMax;
        tigerDog.setFullness(fullness);
    }

    //make happy for a second
    happyFlash();
}

function playPet() {
    //called in playbutton on event, increase play bar on click
    var play = tigerDog.getPlay();
    var playMax = tigerDog.getPlayMax();

    if (play < playMax) {
        play += 15;
        tigerDog.setPlay(play);
    }
    else {
        play = playMax;
        tigerDog.setPlay(play);
    }
    
   //make happy for a second
   happyFlash();

}

function sprayPet() {
    //called in spraybutton on event, just flash sad, do nothing else
    tigerDog.setImage('sad');                 //everytime we spray the pet, show the sad image for a second!
    setTimeout(timerCheckEmoteState, 1000);     //then reset the image to actual happiness level
}


//Add Function to the buttons

//Increase Fullness on click on feeding button
var foodButton = document.getElementById('btn-feed');
foodButton.addEventListener("click", feedPet, false);

//Increase Play on click on play button
var playButton = document.getElementById('btn-play');
playButton.addEventListener("click", playPet, false);

//show sad pet on click on spray button
var sprayButton = document.getElementById('btn-spray');
sprayButton.addEventListener("click", sprayPet, false);



//Call Functions

//tigerDog.setBaseStats();
testSad();
//testNeutral();
//testHungry();

function runGame() {

    //these are functions to update the pet's emote state
    tigerDog.happinessCalculator();
    tigerDog.checkEmoteState();
}

runGame(); //call it once to make sure it starts right
setInterval(runGame, 2000); //check the emote state every 2 seconds, update if necessary

console.log("fullness: " + tigerDog.getFullness() + " happiness " + tigerDog.getHappiness());
